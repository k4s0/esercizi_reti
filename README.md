﻿# Assignment Reti AD:2017/18

## usefull link
http://www.cs.rochester.edu/~kshen/csc257-fall2011/assignments/assignment3.html
http://www.cs.ucf.edu/~czou/CDA4527/PA-2.html
https://www.experts-exchange.com/questions/24163288/How-to-implement-an-ARQ-stop-and-Wait-Protocol.html

## stop and wait protocol over RDT
http://coryg89.github.io/networking.html

## go back n protocol over RDT

### best
https://github.com/jyingt/go-back-n/blob/master/gbn.c
https://github.com/wasifaleem/reliable-transport-protocols/blob/rtt-estimation/src/sr.cpp

### good
https://stackoverflow.com/questions/10201718/kurose-simulated-network-environment-go-back-n
https://github.com/wasifaleem/reliable-transport-protocols/blob/rtt-estimation/src/gbn.cpp
